const path = require('path');
const merge = require('webpack-merge');
const webpackNodeExternals = require('webpack-node-externals');
const baseConfig = require('./webpack.base.js');

const config = {
  /* Tell Webpack that we're building a bundle for NodeJS, not the browser. */
  target: 'node',

  /* Root file of our application */
  entry: './src/index.js',

  /* Tell Webpack where to put the output file that is generated. */
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'build'),
  },

  externals: [webpackNodeExternals()],
};

module.exports = merge(baseConfig, config);
