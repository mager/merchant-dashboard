/* async...await support */
import 'babel-polyfill';
import express from 'express';
import { matchRoutes } from 'react-router-config';
import proxy from 'express-http-proxy';

import routes from './client/routes';
import renderer from './helpers/renderer';
import createStore from './helpers/createStore';

const app = express();

app.use(
  '/api',
  proxy('http://react-ssr-api.herokuapp.com', {
    proxyReqOptDecorator(opts) {
      opts.headers['x-forwarded-host'] = 'localhost:3000';
      return opts;
    },
  }),
);
app.use(express.static('public'));
app.get('*', async (req, res) => {
  /* Pass in the original request so the cookie can be forwarded. */
  const store = createStore(req);

  /* Decide which components need to be renderered */
  const promises = matchRoutes(routes, req.path)
    .map(({ route }) => {
      /* Make sure the loadData function exists */
      return route.loadData ? route.loadData(store) : undefined;
    })
    /* Resolve the promises even if they fail */

    .map(promise => {
      if (promise) {
        return new Promise((resolve, reject) => {
          promise.then(resolve).catch(resolve);
        });
      }
    });

  /* Fetch all the initial data */
  await Promise.all(promises);

  /* Handle responses */
  const context = {};

  /* The store has data now. */
  const content = renderer(req, store, context);

  /* Try to redirect */
  if (context.url) {
    return res.redirect(301, context.url);
  }

  /* Handle everything else */
  if (context.notFound) {
    res.status(404);
  }

  /* Send the content to the client */
  res.send(content);
});

app.listen(3000, () => {
  console.log('Listening on port 3000.');
});
