import React from 'react';
import App from './App';
import HomePage from './pages/HomePage';
import UsersPage from './pages/UsersPage';
import AdminsPage from './pages/AdminsPage';
import LoginPage from './pages/LoginPage';
import NotFoundPage from './pages/NotFoundPage';

const routes = [
  {
    ...HomePage,
    path: '/',
    exact: true,
  },
  {
    ...LoginPage,
    path: '/login',
  },
  {
    ...UsersPage,
    path: '/users',
  },
  {
    ...AdminsPage,
    path: '/admins',
  },
  {
    ...NotFoundPage,
    path: '',
  },
];

export default [
  {
    ...App,
    routes,
  },
];
