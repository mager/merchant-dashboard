import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';

import users from './users';
import auth from './auth';
import admins from './admins';

export default combineReducers({
  admins,
  auth,
  users,
  form,
});
