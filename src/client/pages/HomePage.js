import React, { Component } from 'react';

class HomePage extends Component {
  render() {
    return (
      <section className="section">
        <h3 className="title">Welcome!</h3>
        <button className="button" onClick={() => console.log('click me')}>
          Press me
        </button>
      </section>
    );
  }
}

export default {
  component: HomePage,
};
