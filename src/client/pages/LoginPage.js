import React, { Component } from 'react';
import { connect } from 'react-redux';
import { signIn } from '../actions/auth';
import LoginForm from '../components/forms/LoginForm';

class LoginPage extends Component {
  handleLogin = data => {
    const { signIn } = this.props;

    signIn(data);
  };

  render() {
    return (
      <section className="section">
        <h3 className="title">Login</h3>
        <LoginForm onSubmit={this.handleLogin} />
      </section>
    );
  }
}

export default {
  component: connect(undefined, { signIn })(LoginPage),
};
