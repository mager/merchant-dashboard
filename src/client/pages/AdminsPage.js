import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchAdmins } from '../actions';
import requireAuth from '../components/hocs/requireAuth';

class AdminsPage extends Component {
  componentDidMount() {
    this.props.fetchAdmins();
  }

  renderUsers() {
    return this.props.admins.map(admin => {
      return <li key={admin.id}>{admin.name}</li>;
    });
  }

  render() {
    return (
      <section className="section">
        <div className="content">
          <h3 className="title">Protected list of admins</h3>
          <ul>{this.renderUsers()}</ul>
        </div>
      </section>
    );
  }
}

function mapStateToProps({ admins }) {
  return {
    admins,
  };
}

export default {
  loadData: ({ dispatch }) => dispatch(fetchAdmins()),
  component: connect(mapStateToProps, { fetchAdmins })(requireAuth(AdminsPage)),
};
