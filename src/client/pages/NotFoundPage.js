import React, { Component } from 'react';

const NotFoundPage = ({ staticContext = {} }) => {
  staticContext.notFound = true;

  return (
    <section className="section">
      <article className="message is-warning">
        <div className="message-header">
          <p>Whoops</p>
        </div>
        <div className="message-body">This page does not exist.</div>
      </article>
    </section>
  );
};

export default {
  component: NotFoundPage,
};
