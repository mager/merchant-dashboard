export const SIGN_IN = 'signIn';

export const signIn = data => async (dispatch, getState, api) => {
  let response = await api.post('/partner/login', data);
  console.log(response);
};
