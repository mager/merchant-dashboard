export const FETCH_USERS = 'fetch_users';

/* `api` is an axios instance with an updated `baseURL` */
export const fetchUsers = () => async (dispatch, getState, api) => {
  const payload = await api('/users');

  dispatch({
    type: FETCH_USERS,
    payload,
  });
};

export const FETCH_CURRENT_USER = 'fetch_current_user';

export const fetchCurrentUser = () => async (dispatch, getState, api) => {
  const payload = await api.get('/current_user');

  dispatch({
    type: FETCH_CURRENT_USER,
    payload,
  });
};

export const FETCH_ADMINS = 'fetch_adins';

export const fetchAdmins = () => async (dispatch, getState, api) => {
  const payload = await api.get('/admins');

  dispatch({
    type: FETCH_ADMINS,
    payload,
  });
};
