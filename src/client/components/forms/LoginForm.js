import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';

import Input from '../Input';

class LoginForm extends Component {
  render() {
    const { handleSubmit, pristine, reset, submitting } = this.props;

    return (
      <div className="LoginForm">
        <form onSubmit={handleSubmit}>
          <Field
            name="email"
            component={Input}
            type="text"
            placeholder="Email"
          />
          <Field
            component={Input}
            type="password"
            name="password"
            placeholder="Password"
          />
          <div className="field">
            <div className="control">
              <button className="button is-link" type="submit">
                Sign In
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default reduxForm({ form: 'login' })(LoginForm);
