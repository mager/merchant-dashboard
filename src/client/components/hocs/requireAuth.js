import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

export default ChildComponent => {
  class RequireAuth extends Component {
    render() {
      switch (this.props.auth) {
        case false:
          /* Not authed. Redirrect them to login. */
          return <Redirect to="/" />;
        case null:
          /* Still figuring it out. */
          return <div>Loading...</div>;
        default:
          /* Successfully authed. Forward props on to the child. */
          return <ChildComponent {...this.props} />;
      }
    }
  }

  function mapStateToProps({ auth }) {
    return { auth };
  }

  return connect(mapStateToProps)(RequireAuth);
};
