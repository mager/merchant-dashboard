import React, { Component } from 'react';

class Input extends Component {
  render() {
    const { type, placeholder, name, input } = this.props;
    return (
      <div className="field">
        <label className="label">{name}</label>
        <div className="control">
          <input
            className="input"
            type={type}
            placeholder={placeholder}
            {...input}
          />
        </div>
      </div>
    );
  }
}

export default Input;
