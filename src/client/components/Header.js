import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';

const Header = ({ auth }) => {
  console.log('My auth status is:', auth);

  const authButton = auth ? (
    <a href="/api/logout" className="navbar-item">
      Logout
    </a>
  ) : (
    <Link to="/login" className="navbar-item">
      Login
    </Link>
  );

  return (
    <nav className="navbar" role="navigation" aria-label="main navigation">
      <div className="navbar-brand">
        <Link to="/" className="navbar-item">
          Home
        </Link>
      </div>
      <div className="navbar-menu is-active">
        <div className="navbar-end">
          <Link to="/users" className="navbar-item">
            Users
          </Link>
          <Link to="/admins" className="navbar-item">
            Admins
          </Link>
          {authButton}
        </div>
      </div>
    </nav>
  );
};

export default connect(({ auth }) => {
  return { auth };
})(Header);
