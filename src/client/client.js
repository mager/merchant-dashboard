/* Startup point for the client-side application */
import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import HomePage from './pages/HomePage';
import { BrowserRouter } from 'react-router-dom';
import { createLogger } from 'redux-logger';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { renderRoutes } from 'react-router-config';
import axios from 'axios';

import routes from './routes';
import reducers from './reducers';

const logger = createLogger({
  level: 'log',
});

const axiosInstance = axios.create({
  baseURL: 'http://mager.postmated.com/v1',
});

axiosInstance.interceptors.request.use(
  config => {
    // Do something before request is sent
    console.log('yo?');
    console.log(config);
    config.params = config.params || {};
    config.params.client = 'merchant.web';
    config.params.version = '0.0.0';

    return config;
  },
  error => {
    // Do something with request error
    return Promise.reject(error);
  },
);

const store = createStore(
  reducers,
  window.INITIAL_STATE,
  applyMiddleware(thunk.withExtraArgument(axiosInstance), logger),
);

ReactDOM.hydrate(
  <Provider store={store}>
    <BrowserRouter>
      <div>{renderRoutes(routes)}</div>
    </BrowserRouter>
  </Provider>,
  document.querySelector('#root'),
);
