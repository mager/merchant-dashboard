module.exports = {
  /* Run Babel on every file it runs through */
  module: {
    rules: [
      {
        /* Only apply Babel to JS files */
        test: /\.js?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        options: {
          presets: [
            'es2015',
            /* Supports async/await */
            'stage-0',
            /* Converts JSX files to regular JS */
            'react',
            [
              'env',
              {
                targets: {
                  /* We only care about the last 2 versions */
                  browsers: ['last 2 versions'],
                },
              },
            ],
          ],
          plugins: ['transform-class-properties'],
        },
      },
    ],
  },
};
